<?php 

include 'vendor/autoload.php';
$constants = include('constants.php');
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();
$estate = 'cbp';
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= strtoupper($estate) ?>: Estate Map</title>
    <!-- <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}"> -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

    <script
      src="https://code.jquery.com/jquery-3.2.1.min.js"
      integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
      crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <style>
        #map { height: 100%; }
        html, body { height: 100%; margin: 0; padding: 0; }
        #control-panel { width: 20%; margin-top: 10px; margin-left: 7px; }
        .panel { border-radius: 0; }
        .panel-heading { border-radius: 0; }
        .panel label { margin-bottom: 0; font-size: 14px; font-weight: bold; }
    </style>
</head>
<body>
    <!--
 @if (count($layers) > 0)
    <div id="control-panel" class="panel panel-primary">
        <div class="panel-heading"><h3 class="panel-title">Control Panel</h3></div>
        <div class="panel-body">
            <h4>Markers</h4>

            <table>
                <tbody>
                @foreach ($layers as $layer)
                    <tr>
                        <td width="15%">
                            <input type="checkbox" id="{{ $layer->layer_type->layer_type_name }}" name="{{ $layer->layer_type->layer_type_name }}" onclick="saveMarker('{{ $layer->layer_type->layer_type_name }}')" />
                        </td>
                        <td><label for="{{ $layer->layer_type->layer_type_name }}">{{ $layer->layer_name }}</label></td>
                    </tr>
                @endforeach
                <tr>
                    <td><input type="checkbox" id="traffic" name="traffic" onclick="toggleTrafficLayer()" /></td>
                    <td><label for="traffic">Traffic Indicator</label> </td>
                </tr>
                <tr>
                    <td><input type="checkbox" id="area" name="area" onclick="toggleEstateArea()" /></td>
                    <td><label for="area">Estate Area</label> </td>
                </tr>
                </tbody>
            </table>

            <div class="clearfix">&nbsp;</div>

            <h4>Style</h4>

            <div class="row">
                <div class="form-group">
                    <label for="estate_latitude" class="col-sm-3 col-form-label">Format: </label>
                    <div class="col-sm-9">
                        <select id="style-selector" class="selector-control">
                            <?php
                            foreach($constants['map_styles'] as $key => $value):
                             ?>
                             <option value="<?= $key ?>"><?= $value ?></option>
                             <?php endforeach; ?>

                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif -->

<div id="map" data-id="<?= $estate ?>"></div>
<script>
    var map, infoWindow, user, trafficLayer, estateArea;
    var code                       = $('#map').attr('data-id');
    var api_estate                 = "<?php echo getenv('BASE_API_URL') ?>" + 'estate.php?code=' + code;
    var api_categories             = "<?php echo getenv('BASE_API_URL') ?>" + 'categories.php?code=' + code;
    var residential_sale_array     = [];
    var office_space_sale_array    = [];
    var transportation_stop_array  = [];
    var office_space_lease_array   = [];
    var bank_atm_array             = [];
    var parking_area_array         = [];
    var retail_space_lease_array   = [];
    var iconBase                   = '<?= getenv("IMAGES_URL") ?>';
    var icons                      = {
        sale_available:    { icon: iconBase + 'residential_sale.png' },
        sold_out:          { icon: iconBase + 'residential_sale.png' },
        lease_available:   { icon: iconBase + 'office_lease.png' },
        leased_out:        { icon: iconBase + 'office_lease.png' },
        parking_available: { icon: iconBase + 'parking_available.png' },
        parking_full:      { icon: iconBase + 'parking_full.png' },
        bus:               { icon: iconBase + 'bus_stop.png' },
        taxi:              { icon: iconBase + 'taxi_stop.png' },
        jeep:              { icon: iconBase + 'jeep_stop.gif' },
        puv:               { icon: iconBase + 'puv_terminal.png' },
        atm:               { icon: iconBase + 'atm.png' },
        user:              { icon: iconBase + 'user_location.png' }
    };
    var styles = {
        default: [
            { featureType: "poi.business", stylers: [{visibility: "off"}] },
            { featureType: "poi.park", elementType: "labels.text", stylers: [{visibility: "off"}] }
        ],
        silver: [
            { elementType: 'geometry', stylers: [{color: '#f5f5f5'}] },
            { elementType: 'labels.icon', stylers: [{visibility: 'off'}] },
            { elementType: 'labels.text.fill', stylers: [{color: '#616161'}] },
            { elementType: 'labels.text.stroke', stylers: [{color: '#f5f5f5'}] },
            { featureType: 'administrative.land_parcel', elementType: 'labels.text.fill', stylers: [{color: '#bdbdbd'}] },
            { featureType: 'poi', elementType: 'geometry', stylers: [{color: '#eeeeee'}] },
            { featureType: 'poi', elementType: 'labels.text.fill', stylers: [{color: '#757575'}] },
            { featureType: 'poi.park', elementType: 'geometry', stylers: [{color: '#e5e5e5'}] },
            { featureType: 'poi.park', elementType: 'labels.text.fill', stylers: [{color: '#9e9e9e'}] },
            { featureType: 'road', elementType: 'geometry', stylers: [{color: '#ffffff'}] },
            { featureType: 'road.arterial', elementType: 'labels.text.fill', stylers: [{color: '#757575'}] },
            { featureType: 'road.highway', elementType: 'geometry', stylers: [{color: '#dadada'}] },
            { featureType: 'road.highway', elementType: 'labels.text.fill', stylers: [{color: '#616161'}] },
            { featureType: 'road.local', elementType: 'labels.text.fill', stylers: [{color: '#9e9e9e'}] },
            { featureType: 'transit.line', elementType: 'geometry', stylers: [{color: '#e5e5e5'}] },
            { featureType: 'transit.station', elementType: 'geometry', stylers: [{color: '#eeeeee'}] },
            { featureType: 'water', elementType: 'geometry',  stylers: [{color: '#c9c9c9'}] },
            { featureType: 'water', elementType: 'labels.text.fill', stylers: [{color: '#9e9e9e'}] }
        ],
        night: [
            { elementType: 'geometry', stylers: [{color: '#242f3e'}] },
            { elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}] },
            { elementType: 'labels.text.fill', stylers: [{color: '#746855'}] },
            { featureType: 'administrative.locality', elementType: 'labels.text.fill', stylers: [{color: '#d59563'}] },
            { featureType: 'poi', elementType: 'labels.text.fill', stylers: [{color: '#d59563'}] },
            { featureType: 'poi.park', elementType: 'geometry', stylers: [{color: '#263c3f'}] },
            { featureType: 'poi.park', elementType: 'labels.text.fill', stylers: [{color: '#6b9a76'}] },
            { featureType: 'road', elementType: 'geometry', stylers: [{color: '#38414e'}] },
            { featureType: 'road', elementType: 'geometry.stroke', stylers: [{color: '#212a37'}] },
            { featureType: 'road', elementType: 'labels.text.fill', stylers: [{color: '#9ca5b3'}] },
            { featureType: 'road.highway', elementType: 'geometry', stylers: [{color: '#746855'}] },
            { featureType: 'road.highway', elementType: 'geometry.stroke', stylers: [{color: '#1f2835'}] },
            { featureType: 'road.highway', elementType: 'labels.text.fill', stylers: [{color: '#f3d19c'}] },
            { featureType: 'transit', elementType: 'geometry', stylers: [{color: '#2f3948'}] },
            { featureType: 'transit.station', elementType: 'labels.text.fill', stylers: [{color: '#d59563'}] },
            { featureType: 'water', elementType: 'geometry', stylers: [{color: '#17263c'}] },
            { featureType: 'water', elementType: 'labels.text.fill', stylers: [{color: '#515c6d'}] },
            { featureType: 'water', elementType: 'labels.text.stroke', stylers: [{color: '#17263c'}] }
        ],
        retro: [
            { elementType: 'geometry', stylers: [{color: '#ebe3cd'}] },
            { elementType: 'labels.text.fill', stylers: [{color: '#523735'}] },
            { elementType: 'labels.text.stroke', stylers: [{color: '#f5f1e6'}] },
            { featureType: 'administrative', elementType: 'geometry.stroke', stylers: [{color: '#c9b2a6'}] },
            { featureType: 'administrative.land_parcel', elementType: 'geometry.stroke', stylers: [{color: '#dcd2be'}] },
            { featureType: 'administrative.land_parcel', elementType: 'labels.text.fill', stylers: [{color: '#ae9e90'}] },
            { featureType: 'landscape.natural', elementType: 'geometry', stylers: [{color: '#dfd2ae'}] },
            { featureType: 'poi', elementType: 'geometry', stylers: [{color: '#dfd2ae'}] },
            { featureType: 'poi', elementType: 'labels.text.fill', stylers: [{color: '#93817c'}] },
            { featureType: 'poi.park', elementType: 'geometry.fill', stylers: [{color: '#a5b076'}] },
            { featureType: 'poi.park', elementType: 'labels.text.fill', stylers: [{color: '#447530'}] },
            { featureType: 'road', elementType: 'geometry', stylers: [{color: '#f5f1e6'}] },
            { featureType: 'road.arterial', elementType: 'geometry', stylers: [{color: '#fdfcf8'}] },
            { featureType: 'road.highway', elementType: 'geometry', stylers: [{color: '#f8c967'}] },
            { featureType: 'road.highway', elementType: 'geometry.stroke', stylers: [{color: '#e9bc62'}] },
            { featureType: 'road.highway.controlled_access', elementType: 'geometry', stylers: [{color: '#e98d58'}] },
            { featureType: 'road.highway.controlled_access', elementType: 'geometry.stroke', stylers: [{color: '#db8555'}] },
            { featureType: 'road.local', elementType: 'labels.text.fill', stylers: [{color: '#806b63'}] },
            { featureType: 'transit.line', elementType: 'geometry', stylers: [{color: '#dfd2ae'}] },
            { featureType: 'transit.line', elementType: 'labels.text.fill', stylers: [{color: '#8f7d77'}] },
            { featureType: 'transit.line', elementType: 'labels.text.stroke', stylers: [{color: '#ebe3cd'}] },
            { featureType: 'transit.station', elementType: 'geometry', stylers: [{color: '#dfd2ae'}] },
            { featureType: 'water', elementType: 'geometry.fill', stylers: [{color: '#b9d3c2'}] },
            { featureType: 'water', elementType: 'labels.text.fill', stylers: [{color: '#92998d'}] }
        ],
        hiding: [
            { featureType: 'poi.business', stylers: [{visibility: 'off'}] },
            { featureType: 'transit', elementType: 'labels.icon', stylers: [{visibility: 'off'}] }
        ],
        clean: [
            { featureType: "administrative.land_parcel",  elementType: "labels", stylers: [{visibility: "off"}] },
            { featureType: "poi", "elementType": "labels.text", stylers: [{visibility: "off"}] },
            { featureType: "poi.business", stylers: [{visibility: "off"}]},
            { featureType: "road", elementType: "labels.icon", stylers: [{visibility: "off"}] },
            { featureType: "road.local", elementType: "labels", stylers: [{visibility: "off"}] },
            { featureType: "transit", stylers: [{visibility: "off"}] }
        ]
    };

    function initMap() {
        $.getJSON(api_estate, function (estate) {
            var latitude   = JSON.parse(estate.data.estate_latitude);
            var longitude  = JSON.parse(estate.data.estate_longitude);
            var checked    = 0;
            var layers     = [];
            var mapOptions = {
                center: { lat: latitude, lng: longitude },
                zoom: 16,
                mapTypeControl: false
            };

            $.getJSON(api_categories, function(category) {
                // set categories in array format
                for (var i = 0; i < category.data.length; i++) {
                    layers.push(category.data[i].category_type);
                }

                // console.log(layers);

                map = new google.maps.Map(document.getElementById('map'), mapOptions);
                infoWindow = new google.maps.InfoWindow;

            //     // Add a marker control panel to the map.
            //     var mapControl  = document.getElementById('control-panel');
            //     map.controls[google.maps.ControlPosition.TOP_LEFT].push(mapControl);

                // Add a style controller to the map.
                // Set the map's style to the initial value of the selector.
                // var styleSelector = document.getElementById('style-selector');

                // if (localStorage.getItem('mapStyle')) {
                //     map.setOptions({styles: styles[localStorage.getItem('mapStyle')]});
                //     document.getElementById('style-selector').value = localStorage.getItem('mapStyle');
                // }else {
                //     map.setOptions({styles: styles[styleSelector.value]});
                // }

                // // Apply new JSON when the user selects a different style.
                // styleSelector.addEventListener('change', function() {
                //     localStorage.setItem('mapStyle', styleSelector.value);
                //     map.setOptions({styles: styles[localStorage.getItem('mapStyle')]});
                // });

                // displays selected markers
                for (var index = 0; index < layers.length; ++index) {
                    // sets checkbox to be checked by default
                    // if (!localStorage.getItem(layers[index])) { document.getElementById(layers[index]).checked = true; }

                    // // saves checkbox status in local storage
                    // var layer = document.getElementById(layers[index]).name;
                    markers(layers[index]);

                    // counts number of selected checkboxes
                    // if (document.getElementById(layers[index]).checked) { checked++; }
                }

                // // refresh parking areas every 15 seconds
                setInterval(function() { markers('parking-area'); removeMarkers('parking-area'); }, 15000);

                // // refresh other markers every 5 minutes
                setInterval(function() {
                    for (index = 0; index < layers.length; ++index) {
                        if (layers[index] != 'parking-area') {
                            markers(layers[index]); removeMarkers(layers[index]);
                        }
                    }
                }, 300000);

                // // updates traffic layer
                if (trafficIndicator()) {
                    setInterval(function () { trafficIndicator(); }, 15000); // refresh traffic layer every 15 seconds
                }

                // if (checked == 0 ) { $('#modalLayers').modal('show'); }
				
				// shows highlighted estate area
				estateAreaIndicator();
            });
        });
    }

    // sets marker inside layers
    function constants(layer) {
        var layerArray = null;

             if (layer == 'residential-sale')    { layerArray = residential_sale_array; }
        else if (layer == 'office-space-sale')   { layerArray = office_space_sale_array; }
        else if (layer == 'office-space-lease')  { layerArray = office_space_lease_array; }
        else if (layer == 'transportation-stop') { layerArray = transportation_stop_array; }
        else if (layer == 'bank-atm')            { layerArray = bank_atm_array; }
        else if (layer == 'parking-area')        { layerArray = parking_area_array; }
        else if (layer == 'retail-space-lease')  { layerArray = retail_space_lease_array; }

        return layerArray;
    }

    // saves checkbox data in local storage
    // function saveMarker(layer) {
    //     var checkbox = document.getElementById(layer);

    //     localStorage.setItem(layer, checkbox.checked);

    //     if (document.getElementById(layer).checked === true) markers(layer);
    //     else removeMarkers(layer);
    // }

    // hides selected markers
    function removeMarkers(layer) {
        var layerArray = constants(layer);

        for(var i = 0; i < layerArray.length; i++){
            layerArray[i].setMap(null);
        }
    }

    // get & shows markers
    function markers(layer) {
        var api_markers = '<?php echo getenv("BASE_API_URL");?>markers.php?code=' + code + '&category=' + layer;
        // var checked     = JSON.parse(localStorage.getItem(layer));
        var layerArray  = constants(layer);

            $.getJSON(api_markers, function (markers) {
                Array.prototype.forEach.call(markers.data, function (marker) {
                    var code           = marker.category_code;
                    var key            = marker.category_type;
                    var name           = marker.marker_name;
                    var description    = marker.marker_description;
                    var address        = marker.marker_address;
                    var contact_person = (marker.marker_contact_person) ?marker.marker_contact_person: 'N/A' ;
                    var contact_number = (marker.marker_contact_number) ?marker.marker_contact_number: 'N/A' ;
                    var contact_email  = (marker.marker_contact_email) ?marker.marker_contact_email: 'N/A' ;
                    var icon           = marker.marker_icon;
                    var status         = marker.marker_status;
                    var point          = new google.maps.LatLng(
                            parseFloat(marker.marker_latitude),
                            parseFloat(marker.marker_longitude)
                    );

                    // console.log(marker)

                    // info window
                    var infoWinContent = document.createElement('div');
                    var strong         = document.createElement('strong');
                    strong.textContent = name;
                    infoWinContent.appendChild(strong);
                    infoWinContent.appendChild(document.createElement('br'));

                    var text         = document.createElement('text');
                    text.textContent = address;
                    infoWinContent.appendChild(text);
                    infoWinContent.appendChild(document.createElement('br'));

                    if (code == 'sale' || code == 'retail') {
                        var text         = document.createElement('text');
                        text.textContent = 'Contact Person: ' + contact_person;
                        infoWinContent.appendChild(text);
                        infoWinContent.appendChild(document.createElement('br'));
                        var text         = document.createElement('text');
                        text.textContent = 'Contact Number: ' + contact_number;
                        infoWinContent.appendChild(text);
                        infoWinContent.appendChild(document.createElement('br'));
                        var text         = document.createElement('text');
                        text.textContent = 'Contact Email: ' + contact_email;
                        infoWinContent.appendChild(text);
                        infoWinContent.appendChild(document.createElement('br'));
                    }

                    if (code == 'parking') {
                        var text         = document.createElement('text');
                        text.textContent = 'Advisory: ' + description;
                        infoWinContent.appendChild(text);
                        infoWinContent.appendChild(document.createElement('br'));
                    }

                    if (code != 'atm' && code != 'stop') {
                        var text         = document.createElement('text');
                        text.textContent = 'Status: ' + status;
                        infoWinContent.appendChild(text);
                    }

                        var markerArray = new google.maps.Marker({
                            map: map,
                            position: point,
                            icon: icons[icon].icon
                        });

                        layerArray.push(markerArray);
                        bindInfoWindow(markerArray, map, infoWindow, infoWinContent);
                })
            });
    }

    function bindInfoWindow(marker, map, infoWindow, infoWinContent) {
        marker.addListener('click', function() {
            if (marker.getAnimation() != null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }

            infoWindow.setContent(infoWinContent);
            infoWindow.open(map, marker);
        });
    }

    function trafficIndicator() {
        trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(map);
    }
 

    function estateAreaIndicator() {
        var api_data = '<?php echo getenv("BASE_API_URL")?>' + 'coordinates.php?code=' + code;

        $.getJSON(api_data, function (coordinates) {

            var data = coordinates.data;
            var dataArray = [];
            var estateCoordinates = [];

            for (var i = 0; i < data.length; i++) {
                dataArray = { lat: parseFloat(coordinates.data[i].latitude), lng: parseFloat(coordinates.data[i].longitude) };
                estateCoordinates.push(dataArray);
            }

            // var checked = JSON.parse(localStorage.getItem('area'));
            estateArea = new google.maps.Polygon({
                path: estateCoordinates,
                geodesic: true,
                strokeColor: '#1b1e21', //#d6d8d9
                strokeOpacity: 1.0,
                strokeWeight: 2,
                fillColor: '#155724',
                fillOpacity: 0.15
            });
            estateArea.setMap(map);

            // if (localStorage.getItem('area')) {
            //     if (document.getElementById('area').checked = checked) 
            //         estateArea.setMap(map);
            // }else {
            //     if (document.getElementById('area').checked = true) estateArea.setMap(map);
            // }
        });

    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqLv54WPwRDGjj-b1nH8i1x3mvPj_tKJs&callback=initMap" async defer></script>
</body>
</html>

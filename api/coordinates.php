<?php 

include '../vendor/autoload.php';

use GuzzleHttp\Client;
$dotenv = new Dotenv\Dotenv('../');
$dotenv->load();

$token = getenv('MAP_API_TOKEN');

$client = new GuzzleHttp\Client(['base_uri' => getenv('API_URL')]);

$headers = [
    'Authorization' => 'Bearer ' . $token,        
    'Accept'        => 'application/json',
];

try {
    $response = $client->request('GET', 'estate/coordinates/' . @$_GET['code'], [
            'headers' => $headers
        ]);
} catch (Exception $e) {
    echo $e->getMessage(); die();
}

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

$res = $response->getBody();
 
echo($res); die();
 
<?php

return [
    'properties'     => [ 'sale' => true, 'lease' => true ],
    'parking-status' => [ 'parking_available' => 'Available', 'parking_status' => 'Full' ],
    'sale-status'    => [ 'sale_available' => 'Available', 'sold_out' => 'Sold Out' ],
    'lease-status'   => [ 'lease_available' => 'Available', 'leased_out' => 'Leased Out' ],
    'stop-status'    => [ 'bus' => 'Bus Stop', 'taxi' => 'Taxi Stop', 'jeep' => 'Jeepney Stop', 'puv' => 'PUV Terminal' ],
    'atm-status'     => [ 'atm' => 'Bank ATM' ],
    'map-styles'     => [
        'default' => 'Default',
        'silver'  => 'Silver',
        'night'   => 'Night',
        'retro'   => 'Retro',
        'clean'   => 'Clean',
        'hiding'  => 'Hide Features'
    ] 
];